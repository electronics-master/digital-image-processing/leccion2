#!/usr/bin/env python3
#
# Pinhole projection exercise
# 
# In this exercise you must rotate an image in the 3D space, and then
# project back the result to the 2D plane by using the pinhole model
#

import cv2
import numpy as np

img = cv2.imread('messi5.jpg',0)
rows,cols = img.shape

# Focal length of the pinhole camera
f = 500

#####################
# Constant matrices
####################

# Move the image into a 3D space, and center in the origin
to3d = np.float32([[1, 0, -cols/2],
                   [0, 1, -rows/2],
                   [0, 0, 1],
                   [0, 0, 1]])

# Move the image to the virtual image plane
t = np.float32([[1, 0, 0, 0],
                [0, 1, 0, 0],
                [0, 0, 1, f],
                [0, 0, 0, 1]])

# Project the image to the 2D plane using the pinhole model
pinHole = np.float32([[f, 0, cols/2, 0],
                      [0, f, rows/2, 0],
                      [0, 0, 1, 0]])

finished = False
while not finished:
    #################################################################
    # FILLME:
    #
    # -Compute the rotation matrix on X
    # -Compute the rotation matrix on Y
    # -Compute the rotation matrix on Z
    # -Combine rotation matrices
    # -Include the rotation matrix on the final transformation
    #################################################################

    # Generate the final matrix
    M = np.matmul(pinHole, np.matmul(t, to3d))

    # Apply transformation
    dst = cv2.warpPerspective(img, M, (cols,rows), cv2.INTER_LANCZOS4)
    
    cv2.imshow('img',dst)

    # Wait for any keypress or continue
    k = cv2.waitKey(33)
    if k != -1:
        finished = True

cv2.destroyAllWindows()
